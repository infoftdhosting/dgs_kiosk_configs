#!/usr/bin/env ruby

require "httparty"
require "json"

class RebootChecker
  include HTTParty

  def initialize
    @time = Time.now
    @site_id = ENV["SITE_ID"].to_i
  end

  def reboot_if_needed
    response = query_server

    if response["reboot"] == true
      puts "Time to reboot!"
      reboot
    elsif response["reboot"].nil?
      puts "Invalid response, trying again in 30 seconds: #{response.inspect}"
      sleep 30
      reboot_if_needed
    else
      puts "No need to reboot."
    end
  end

  protected

  attr_reader :site_id, :time

  private

  def persistent_lost_connection?
    # Persistent connection if 15 minutes have elapsed.
    time + (15 * 60) < Time.now
  end

  def query_server
    reboot if persistent_lost_connection?

    puts "Querying server for Dashboard ##{site_id}..."
    url = "http://hyperion-dgs-production.herokuapp.com"\
      "/api/dashboard_reboot/#{site_id}"

    self.class.get(url)
  rescue SocketError
    puts "Socket error (likely bad network connection), waiting 30 seconds and trying again."
    sleep 30
    query_server
  end

  def reboot
    puts "Rebooting kiosk..."
    system("sudo reboot")
  end
end

RebootChecker.new.reboot_if_needed

#!/bin/bash

# Move to the configuration folder, reset the folder, pull the latest changes
# from Github
echo "Pulling latest configuration and content from sources..."
cd /Users/kiosk/dgs_kiosk_configs && git reset --hard && git pull origin master --force
cd /Users/kiosk/kiosk_source && git reset --hard && git pull origin master --force

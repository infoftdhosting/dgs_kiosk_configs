# Hyperion Kiosk Config

This repository contains scripts and other collateral that is shared amongst the
Hyperion kiosks. Key scripts are responsible for:

* Executing Cron tasks on a schedule that can be updated by git.
* Downloading new configuration .
* Starting the kiosk and immediately loading Chrome in kiosk mode with the
  proper dashboard. (Using chrome-cli on OSX)
* Disabling right-clicking (Linux only)
* Restarting the machine in the event network access is disabled and/or the
  kiosk receives a signal to restart from the server.

To add items to `dgs` account cron, edit the relevant file, which are called by
cron. These items must be run as `dgs`, to add root-level cron tasks, you must
edit the box directly for security purposes.

* Monthly is the first of the month.
* Every week is on Sunday after midnight.
* Every 12 hours is at 7am and 7pm.

* Reboots every day at 5am

* Download new git configs every 5 minutes.
